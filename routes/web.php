<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/category/list-product', 'CategoryController@index')->name('category.index');
Route::get('category/product/detail', 'ProductController@detail')->name('product.detail');
Route::get('contact', 'ContactController@index')->name('contact');
Route::get('blog', 'BlogController@index')->name('blog');


// Admin

// Route::get('/admin', 'Admin\AdminController@index');
Route::prefix('admin')->group(function () {
    Route::get('/dashboard', 'Admin\AdminController@index')->name('admin');
    Route::get('/category', 'Admin\CategoryController@index')->name('admin.category.index');
    Route::get('/category/create', 'Admin\CategoryController@formCreate')->name('admin.category.formCreate');
    Route::post('/category/create', 'Admin\CategoryController@create')->name('admin.category.create');
    Route::get('/category/edit/{id}', 'Admin\CategoryController@formEdit')->name('admin.category.formEdit');
    Route::post('/category/edit/{id}', 'Admin\CategoryController@edit')->name('admin.category.edit');
    Route::post('/category/delete', 'Admin\CategoryController@delete')->name('admin.category.delete');

    Route::get('/user', 'Admin\UserController@index')->name('admin.user.index');
    Route::get('/user/create', 'Admin\UserController@formCreate')->name('admin.user.formCreate');
    Route::post('/user/create', 'Admin\UserController@create')->name('admin.user.create');
    Route::get('/user/edit/{id}', 'Admin\UserController@formEdit')->name('admin.user.formEdit');
    Route::post('/user/edit/{id}', 'Admin\UserController@edit')->name('admin.user.edit');
    Route::post('/user/delete', 'Admin\UserController@delete')->name('admin.user.delete');

    Route::get('/product', 'Admin\ProductController@index')->name('admin.product.index');
    Route::get('/product/create', 'Admin\ProductController@formCreate')->name('admin.product.formCreate');
    Route::post('/product/create', 'Admin\ProductController@create')->name('admin.product.create');
    Route::get('/product/edit/{id}', 'Admin\ProductController@formEdit')->name('admin.product.formEdit');
    Route::post('/product/edit/{id}', 'Admin\ProductController@edit')->name('admin.product.edit');
    Route::post('/product/delete', 'Admin\ProductController@delete')->name('admin.product.delete');
});