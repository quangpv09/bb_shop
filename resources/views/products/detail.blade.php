@extends('layouts.app')

@section('content')
    <div class="detail-product">
        <div class="header">
            <div class="wrap-breadcrumb">
                <div class="clearfix container">
                    <div class="row main-header">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5  ">
                            <ol class="breadcrumb breadcrumb-arrows">
                                <li><a href="/" target="_self">Trang chủ</a></li>


                                <li><a href="/collections" target="_self">Danh mục</a></li>



                                <li class="active"><span>Tất cả sản phẩm</span></li>



                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        @include('sidebar.sidebar')
                    </div>
                    <div class="col-md-9">
                       <div class="row">
                           <div class="col-md-6">
                               <div class="prod-img">
                                   <img src="https://product.hstatic.net/1000150333/product/upload_dc84aed7c9574b28bd3432c527a1f4fc_master.jpg">
                               </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <div class="product-title">
                                   <h1>Xe đẩy Goodbaby</h1>

                                   <span id="pro_sku">SKU: XDGO-1</span>

                               </div>
                               <div class="product-price" id="price-preview"><span>3,500,000₫</span></div>



                               <form id="add-item-form" action="/cart/add" method="post" class="variants clearfix">
                                   <div class="select clearfix">
                                       <div class="selector-wrapper"><label for="product-select-option-0">Kích thước</label><span class="custom-dropdown custom-dropdown--white"><select class="single-option-selector custom-dropdown__select custom-dropdown__select--white" data-option="option1" id="product-select-option-0"><option value="S">S</option><option value="M">M</option></select></span></div><div class="selector-wrapper"><label for="product-select-option-1">Màu sắc</label><span class="custom-dropdown custom-dropdown--white"><select class="single-option-selector custom-dropdown__select custom-dropdown__select--white" data-option="option2" id="product-select-option-1"><option value="Hồng">Hồng</option></select></span></div><select id="product-select" name="id" style="display:none">

                                           <option value="1009814340">S / Hồng - 3,500,000₫</option>

                                           <option value="1009814341">M / Hồng - 3,500,000₫</option>

                                       </select>
                                   </div>

                                   <div class="select-wrapper ">
                                       <label>Số lượng</label>
                                       <input id="quantity" type="number" name="quantity" min="1" value="1" class="tc item-quantity">
                                   </div>


                                   <div class="row">
                                       <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                           <button id="add-to-cart" class="btn-detail btn-color-add btn-min-width btn-mgt addtocart-modal" name="add">Thêm vào giỏ</button>
                                       </div>

                                       <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                                           <button id="buy-now" class="btn-detail btn-color-buy btn-min-width btn-mgt">Mua ngay</button>
                                       </div>

                                   </div>
                               </form>

                               <div class="pt20">
                                   <!-- Begin social icons -->
                                   <div class="addthis_toolbox addthis_default_style ">

                                       <div class="info-socials-article clearfix">
                                           <div class="box-like-socials-article">
                                               <div class="fb-send" data-href="/products/xe-day-goodbaby">
                                               </div>
                                           </div>
                                           <div class="box-like-socials-article">
                                               <div class="fb-like fb_iframe_widget" data-href="/products/xe-day-goodbaby" data-layout="button_count" data-action="like" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=&amp;container_width=0&amp;href=https%3A%2F%2Fdefault-baby.myharavan.com%2Fproducts%2Fxe-day-goodbaby&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey"><span style="vertical-align: bottom; width: 90px; height: 28px;"><iframe name="f4330820890914" width="1000px" height="1000px" data-testid="fb:like Facebook Social Plugin" title="fb:like Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.0/plugins/like.php?action=like&amp;app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df1b34bc9effd7cc%26domain%3Ddefault-baby.myharavan.com%26origin%3Dhttps%253A%252F%252Fdefault-baby.myharavan.com%252Ffb939473c3bd1%26relation%3Dparent.parent&amp;container_width=0&amp;href=https%3A%2F%2Fdefault-baby.myharavan.com%2Fproducts%2Fxe-day-goodbaby&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey" style="border: none; visibility: visible; width: 90px; height: 28px;" class=""></iframe></span></div>
                                           </div>
                                           <div class="box-like-socials-article">
                                               <div class="fb-share-button fb_iframe_widget" data-href="/products/xe-day-goodbaby" data-layout="button_count" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=0&amp;href=https%3A%2F%2Fdefault-baby.myharavan.com%2Fproducts%2Fxe-day-goodbaby&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey"><span style="vertical-align: bottom; width: 86px; height: 20px;"><iframe name="f39b055ee1895c4" width="1000px" height="1000px" data-testid="fb:share_button Facebook Social Plugin" title="fb:share_button Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.0/plugins/share_button.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Dfa8f1070a8be88%26domain%3Ddefault-baby.myharavan.com%26origin%3Dhttps%253A%252F%252Fdefault-baby.myharavan.com%252Ffb939473c3bd1%26relation%3Dparent.parent&amp;container_width=0&amp;href=https%3A%2F%2Fdefault-baby.myharavan.com%2Fproducts%2Fxe-day-goodbaby&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey" style="border: none; visibility: visible; width: 86px; height: 20px;" class=""></iframe></span></div>
                                           </div>
                                       </div>


                                   </div>
                                   <!-- End social icons -->
                               </div>
                           </div>
                       </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px;">
                                <div role="tabpanel" class="product-comment">
                                    <!-- Nav tabs -->
                                    <ul class="nav visible-lg visible-md" role="tablist">
                                        <li role="presentation" class="active"><a data-spy="scroll" data-target="#mota" href="#mota" aria-controls="mota" role="tab">Mô tả sản phẩm</a></li>

                                        <li role="presentation">
                                            <a data-spy="scroll" href="#binhluan" aria-controls="binhluan" role="tab">Bình luận</a>
                                        </li>


                                        <li role="presentation">
                                            <a data-spy="scroll" href="#like" aria-controls="like" role="tab">Sản phẩm khác</a>
                                        </li>

                                    </ul>
                                    <!-- Tab panes -->

                                    <div id="mota">

                                        <div class="title-bl visible-xs visible-sm" style="display: none">
                                            <h2>Mô tả</h2>
                                        </div>

                                        <div class="product-description-wrapper">

                                            <p>Chăm sóc trẻ sơ sinh là việc không hề đơn giản, nhất là trong việc lựa chọn trang phục cho bé. Làm thế nào để cơ thể bé luôn được giữ ấm, mà lại mang đến cho bé cảm giác dễ chịu khi mặc?</p><p>Sản phẩm áo ghi lê Papa với thiết kế đẹp mắt, chất liệu cotton mềm mại êm ái và đường may tỉ mỉ, khéo léo sẽ đáp ứng các yêu cầu của bạn.</p><p>Sản phẩm được làm chất liệu 100% cotton, thấm hút mồ hôi và thông thoáng, thích hợp cho làn da nhạy cảm của bé. Các đường chỉ cuộn biên không cộm, gồ ghề gây khó chịu cho bé.</p><p>Áo ghi lê Papa cho bé, có nhiều màu và họa tiết dành&nbsp;cho bé từ 0-12 tháng tuổi.</p><p>Áo&nbsp;được thiết kế kiểu cúc cài giữa giúp mẹ mặc cho bé dễ dàng hơn.&nbsp;</p><h4><span>Hướng dẫn sử dụng để áo sơ sinh Papa luôn bền, mới:</span></h4><p>Không giặt với nước nóng quá 40<sup>o</sup>C.</p><p>Không giặt với chất tẩy mạnh.</p><p>Có thể sấy ở nhiệt độ trung bình.</p><p>Ủi với mặt trái của sản phẩm.</p><p>Ủi với nhiệt độ trung bình.</p><p>Không nên vắt sản phẩm.</p><h4>Đặc điểm nổi bật của áo ghi lê bé sơ sinh Papa:</h4><p><span>- Thiết kế và đường may tỉ mỉ, khéo léo</span></p><p>- Các đường nối, đường cuốn biên không bị nổi đường gân gây khó chịu và ngứa da bé. Bé sẽ cảm thấy dễ chịu và thoải mái mà không cảm thấy bị cấn hay cọ xát gây ngứa.&nbsp;</p><p><span>- Chất liệu 100% cotton cao cấp,&nbsp;</span>co giãn giúp làn da thông thoáng và máu lưu thông tốt.&nbsp;</p><p><span>- Thiết kế xinh xắn, nhiều màu sắc bắt mắt, dễ thương.</span></p><p>- Sản phẩm được sản xuất tại Việt Nam.</p><p style="margin: 0px; padding: 8px 0px; color: #4a4a4a; font-family: Arial, 'Open Sans', HelveticaNeue, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18px; text-align: justify; background-color: #ffffff;" data-mce-style="margin: 0px; padding: 8px 0px; color: #4a4a4a; font-family: Arial, 'Open Sans', HelveticaNeue, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18px; text-align: justify; background-color: #ffffff;">Bảng thông tin chọn kích thước:</p><div class="pop_size" style="margin: 0px; padding: 0px; color: #4a4a4a; font-family: Arial, 'Open Sans', HelveticaNeue, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18px; text-align: justify; background-color: #ffffff;" data-mce-style="margin: 0px; padding: 0px; color: #4a4a4a; font-family: Arial, 'Open Sans', HelveticaNeue, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18px; text-align: justify; background-color: #ffffff;"><div class="table_cc" style="margin: 10px auto; padding: 0px 0px 0px 32px; border: 0px solid #fce2ec; min-width: 90%; clear: both; float: left;" data-mce-style="margin: 10px auto; padding: 0px 0px 0px 32px; border: 0px solid #fce2ec; min-width: 90%; clear: both; float: left;"><div class="line_tr line_th " style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left;" data-mce-style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left;"><div class="line_td" style="margin: 0px; padding: 5px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 5px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">Size</div><div class="line_td" style="margin: 0px; padding: 5px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 5px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">Chiều cao</div><div class="line_td td_last" style="margin: 0px; padding: 5px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;" data-mce-style="margin: 0px; padding: 5px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;">Cân nặng</div></div><div class="line_tr " style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left;" data-mce-style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left;"><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">Sơ sinh</div><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">45 - 55 cm</div><div class="line_td td_last" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;">2.3 - 3.6 kg</div></div><div class="line_tr bg_tbcc" style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left; background: #f8f8f8;" data-mce-style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left; background: #f8f8f8;"><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">0 - 3M</div><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">55 - 61 cm</div><div class="line_td td_last" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;">3.6 - 5.7 kg</div></div><div class="line_tr " style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left;" data-mce-style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left;"><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">3 - 6M</div><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">61 - 67 cm</div><div class="line_td td_last" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;">5.7 - 7.5 kg</div></div><div class="line_tr bg_tbcc" style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left; background: #f8f8f8;" data-mce-style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left; background: #f8f8f8;"><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">6 - 9M</div><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">67 - 72 cm</div><div class="line_td td_last" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;">7.5 - 9.3 kg</div></div><div class="line_tr " style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left;" data-mce-style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left;"><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">9 - 12M</div><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">72 - 78 cm</div><div class="line_td td_last" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;">9.3 - 11.0 kg</div></div><div class="line_tr " style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left;" data-mce-style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px 1px 0px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #fce2ec; float: left;"><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">12 - 18M</div><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">78 - 83 cm</div><div class="line_td td_last" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;">11.0 - 12.5 kg</div></div><div class="line_tr bg_tbcc line_last" style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #e7e7e7; float: left; background: #f8f8f8;" data-mce-style="margin: 0px; padding: 0px; width: 547.188px; border-width: 1px; border-style: solid; border-color: #f0f0f0 #f0f0f0 #e7e7e7; float: left; background: #f8f8f8;"><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">18 - 24M</div><div class="line_td" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 1px; border-right-style: solid; border-right-color: #f0f0f0;">83 - 86 cm</div><div class="line_td td_last" style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;" data-mce-style="margin: 0px; padding: 4px 0px; text-align: center; min-width: 33%; float: left; border-right-width: 0px; border-right-style: solid; border-right-color: #fefefe;">12.5 - 13.6 kg</div></div></div></div>

                                        </div>
                                    </div>

                                    <div id="binhluan">
                                        <div class="title-bl">
                                            <h2>Bình luận</h2>
                                        </div>
                                        <div class="product-comment-fb">
                                            <div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; width: 0px; height: 0px;"><div></div></div></div>
                                            <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid_desktop" data-href="https://default-baby.myharavan.com/products/ao-ghi-le-be-trai-papa" data-numposts="5" width="100%" data-colorscheme="light" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;color_scheme=light&amp;container_width=828&amp;height=100&amp;href=https%3A%2F%2Fdefault-baby.myharavan.com%2Fproducts%2Fao-ghi-le-be-trai-papa&amp;locale=vi_VN&amp;numposts=5&amp;sdk=joey&amp;version=v2.0&amp;width=" style="width: 100%;"><span style="vertical-align: bottom; width: 100%; height: 203px;"><iframe name="f3398b76449bdac" width="1000px" height="100px" data-testid="fb:comments Facebook Social Plugin" title="fb:comments Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.0/plugins/comments.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df36983abae7f20c%26domain%3Ddefault-baby.myharavan.com%26origin%3Dhttps%253A%252F%252Fdefault-baby.myharavan.com%252Ff2daaf451255428%26relation%3Dparent.parent&amp;color_scheme=light&amp;container_width=828&amp;height=100&amp;href=https%3A%2F%2Fdefault-baby.myharavan.com%2Fproducts%2Fao-ghi-le-be-trai-papa&amp;locale=vi_VN&amp;numposts=5&amp;sdk=joey&amp;version=v2.0&amp;width=" style="border: none; visibility: visible; width: 100%; height: 203px;" class=""></iframe></span></div>
                                            <!-- script comment fb -->
                                            <script type="text/javascript">(function(d, s, id) {
                                                    var js, fjs = d.getElementsByTagName(s)[0];
                                                    if (d.getElementById(id)) return;
                                                    js = d.createElement(s); js.id = id;
                                                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }(document, 'script', 'facebook-jssdk'));
                                            </script>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12  list-like">
                                <div id="like">
                                    <div class="title-like">
                                        <h2>Sản phẩm khác</h2>
                                    </div>
                                    <div class="row product-list ">
                                            <div class="col-md-3 col-sm-6 col-xs-12 pro-loop">
















                                                <div class="product-block product-resize fixheight" style="height: 281px;">
                                                    <div class="product-img image-resize view view-third" style="height: 187px;">



                                                        <a href="/products/ao-so-sinh-cai-cheo-tay-ngan" title="Áo sơ sinh cài chéo tay ngắn">
                                                            <img class="first-image  has-img" alt=" Áo sơ sinh cài chéo tay ngắn " src="//product.hstatic.net/1000150333/product/upload_e5931f068852418bb0a18096307e62b6_large.jpg">

                                                            <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_3e7375be77c045c697b832dc04d83acc_large.jpg" alt=" Áo sơ sinh cài chéo tay ngắn ">

                                                        </a>
                                                        <div class="actionss">
                                                            <div class="btn-cart-products">
                                                                <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814276)">
                                                                    <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                            <div class="view-details">
                                                                <a href="/collections/hot-products/products/ao-so-sinh-cai-cheo-tay-ngan" class="view-detail">
                                                                    <span><i class="fa fa-clone"> </i></span>
                                                                </a>
                                                            </div>
                                                            <div class="btn-quickview-products">
                                                                <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-cheo-tay-ngan"><i class="fa fa-eye"></i></a>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="product-detail clearfix">


                                                        <!-- sử dụng pull-left -->
                                                        <h3 class="pro-name"><a href="/collections/hot-products/products/ao-so-sinh-cai-cheo-tay-ngan" title="Áo sơ sinh cài chéo tay ngắn">Áo sơ sinh cài chéo tay ngắn </a></h3>
                                                        <div class="pro-prices">
                                                            <p class="pro-price">42,000₫</p>
                                                            <p class="pro-price-del text-left"></p>


                                                        </div>


                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 pro-loop">
















                                                <div class="product-block product-resize fixheight" style="height: 281px;">
                                                    <div class="product-img image-resize view view-third" style="height: 187px;">



                                                        <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" title="Áo sơ sinh cài giữa tay ngắn Carrot">
                                                            <img class="first-image  has-img" alt=" Áo sơ sinh cài giữa tay ngắn Carrot " src="//product.hstatic.net/1000150333/product/upload_4a649361ca15483b908c96246915f175_large.jpg">

                                                            <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_5ec366ba30234f3981325400727fa6a6_large.jpg" alt=" Áo sơ sinh cài giữa tay ngắn Carrot ">

                                                        </a>
                                                        <div class="actionss">
                                                            <div class="btn-cart-products">
                                                                <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                                    <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                            <div class="view-details">
                                                                <a href="/collections/hot-products/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                                    <span><i class="fa fa-clone"> </i></span>
                                                                </a>
                                                            </div>
                                                            <div class="btn-quickview-products">
                                                                <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="product-detail clearfix">


                                                        <!-- sử dụng pull-right -->
                                                        <h3 class="pro-name"><a href="/collections/hot-products/products/ao-so-sinh-cai-giua-tay-ngan-carrot" title="Áo sơ sinh cài giữa tay ngắn Carrot">Áo sơ sinh cài giữa tay ngắn Carrot </a></h3>
                                                        <div class="pro-prices">
                                                            <p class="pro-price">33,000₫</p>
                                                            <p class="pro-price-del text-left"></p>


                                                        </div>


                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 pro-loop">
















                                                <div class="product-block product-resize fixheight" style="height: 281px;">
                                                    <div class="product-img image-resize view view-third" style="height: 187px;">



                                                        <a href="/products/bo-tay-dai-quan-dai-carrot" title="Bộ tay dài quần dài Carrot">
                                                            <img class="first-image  has-img" alt=" Bộ tay dài quần dài Carrot " src="//product.hstatic.net/1000150333/product/upload_0fcd054a19bd49b9812d27e9c0d516e7_large.jpg">

                                                            <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_fa31a29af1e24e2aae3f2a91bfcdcb33_large.jpg" alt=" Bộ tay dài quần dài Carrot ">

                                                        </a>
                                                        <div class="actionss">
                                                            <div class="btn-cart-products">
                                                                <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814296)">
                                                                    <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                            <div class="view-details">
                                                                <a href="/collections/hot-products/products/bo-tay-dai-quan-dai-carrot" class="view-detail">
                                                                    <span><i class="fa fa-clone"> </i></span>
                                                                </a>
                                                            </div>
                                                            <div class="btn-quickview-products">
                                                                <a href="javascript:void(0);" class="quickview" data-handle="/products/bo-tay-dai-quan-dai-carrot"><i class="fa fa-eye"></i></a>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="product-detail clearfix">


                                                        <!-- sử dụng pull-left -->
                                                        <h3 class="pro-name"><a href="/collections/hot-products/products/bo-tay-dai-quan-dai-carrot" title="Bộ tay dài quần dài Carrot">Bộ tay dài quần dài Carrot </a></h3>
                                                        <div class="pro-prices">
                                                            <p class="pro-price">79,000₫</p>
                                                            <p class="pro-price-del text-left"></p>


                                                        </div>


                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 pro-loop">
















                                                <div class="product-block product-resize fixheight" style="height: 281px;">
                                                    <div class="product-img image-resize view view-third" style="height: 187px;">



                                                        <a href="/products/gau-bong-ru-ngu" title="Gấu bông ru ngủ">
                                                            <img class="first-image  has-img" alt=" Gấu bông ru ngủ " src="//product.hstatic.net/1000150333/product/upload_738481f189804db78ed2a4a3f0ce99b3_large.jpg">

                                                            <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_93fb13ebec3b4070ac759e0367a4eda5_large.jpg" alt=" Gấu bông ru ngủ ">

                                                        </a>
                                                        <div class="actionss">
                                                            <div class="btn-cart-products">
                                                                <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814303)">
                                                                    <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                            <div class="view-details">
                                                                <a href="/collections/hot-products/products/gau-bong-ru-ngu" class="view-detail">
                                                                    <span><i class="fa fa-clone"> </i></span>
                                                                </a>
                                                            </div>
                                                            <div class="btn-quickview-products">
                                                                <a href="javascript:void(0);" class="quickview" data-handle="/products/gau-bong-ru-ngu"><i class="fa fa-eye"></i></a>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="product-detail clearfix">


                                                        <!-- sử dụng pull-right -->
                                                        <h3 class="pro-name"><a href="/collections/hot-products/products/gau-bong-ru-ngu" title="Gấu bông ru ngủ">Gấu bông ru ngủ </a></h3>
                                                        <div class="pro-prices">
                                                            <p class="pro-price">210,000₫</p>
                                                            <p class="pro-price-del text-left"></p>


                                                        </div>


                                                    </div>
                                                </div>

                                            </div>
                                    </div>
                                    <script>
                                        var add_to_wishlist = function(){
                                            if(typeof(Storage) !== "undefined")
                                            {
                                                if (localStorage.recently_viewed)
                                                {

                                                    if(localStorage.recently_viewed.indexOf('1st-birthday-princess-basic-party-kit-18-guests') == -1)
                                                        localStorage.recently_viewed = '1st-birthday-princess-basic-party-kit-18-guests_'+localStorage.recently_viewed ;

                                                } else
                                                    localStorage.recently_viewed = '1st-birthday-princess-basic-party-kit-18-guests';
                                            }
                                            else {
                                                console.log('Your Browser does not support storage!');
                                            }
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
{{--    <script src="{{ asset('js/categories.js') }}"></script>--}}
@endsection
