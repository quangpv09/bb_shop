<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

{{--    <!-- Scripts -->--}}
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="header">
            <div class="top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="logo">
                                <img src="{{asset('images/logo.webp')}}">
                            </div>
                        </div>
                        <div class="col-md-5 d-flex" style="justify-content: flex-end">
                            <div class="navholder">
                                <div class="subnav">
                                    <ul>
                                        <li style="padding-left: 0">
                                            <a href="#">
                                                <i class="fa fa-phone" aria-hidden="true"></i>
                                                0123456789
                                            </a>
                                        </li>
                                       @if(Auth::check())
                                       <li>
                                            <a href="#">
                                                {{Auth::user()->email}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                                DANG XUAT
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form>
                                        </li>
                                       @else
                                        <li>
                                            <a href="#">
                                                ĐĂNG KÝ
                                            </a>
                                        </li>
                                        <li style="padding-right: 0">
                                            <a href="#">
                                                ĐĂNG NHẬP
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                                <div class="header-line">
                                    <p>Miễn phí vận chuyển <span>ĐƠN HÀNG TRÊN 900K</span></p>
                                </div>
                            </div>
                            <div class="cart-info">
                                <a href="#" class="cart-link">
                                    <div class="cart-number">0</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-expand-lg navbar-light">
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item">
                                            <a style="padding-left: 0" class="nav-link" href="#">Trang chủ</a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Sản phẩm
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Blog</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Giới thiệu</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Liên hệ</a>
                                        </li>
                                    </ul>
                                    <div class="search-bar">
                                        <form class="form-inline my-2 my-lg-0">
                                            <input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm..." autocomplete="off" aria-label="Search">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </form>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
{{--        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">--}}
{{--            <div class="container">--}}
{{--                <a class="navbar-brand" href="{{ url('/') }}">--}}
{{--                    {{ config('app.name', 'Laravel') }}--}}
{{--                </a>--}}
{{--                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">--}}
{{--                    <span class="navbar-toggler-icon"></span>--}}
{{--                </button>--}}

{{--                <div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
{{--                    <!-- Left Side Of Navbar -->--}}
{{--                    <ul class="navbar-nav mr-auto">--}}

{{--                    </ul>--}}

{{--                    <!-- Right Side Of Navbar -->--}}
{{--                    <ul class="navbar-nav ml-auto">--}}
{{--                        <!-- Authentication Links -->--}}
{{--                        @guest--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
{{--                            </li>--}}
{{--                            @if (Route::has('register'))--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
{{--                                </li>--}}
{{--                            @endif--}}
{{--                        @else--}}
{{--                            <li class="nav-item dropdown">--}}
{{--                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
{{--                                    {{ Auth::user()->name }}--}}
{{--                                </a>--}}

{{--                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
{{--                                    <a class="dropdown-item" href="{{ route('logout') }}"--}}
{{--                                       onclick="event.preventDefault();--}}
{{--                                                     document.getElementById('logout-form').submit();">--}}
{{--                                        {{ __('Logout') }}--}}
{{--                                    </a>--}}

{{--                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">--}}
{{--                                        @csrf--}}
{{--                                    </form>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        @endguest--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </nav>--}}

        <main>
            @yield('content')
        </main>
        <footer id="footer">
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">

                        <div class="col-sm-6 col-md-3 col-xs-12 clear-sm">
                            <div class="widget-wrapper animated">

                                <h3 class="title title_left">Giới thiệu</h3>

                                <div class="inner about_us">

                                    <p class="message">Với hơn 100 nhân viên tư vấn trên mọi phương diện, không chỉ là hướng dẫn và xử lý các vấn đề từ Haravan, chúng tôi luôn mong cùng chia sẻ các kinh nghiệm giúp bạn bán được nhiều hàng hơn.</p>

                                    <ul class="list-unstyled">

                                        <li>
                                            <i class="fa fa-home"></i>56 Vân côi, Q. Tân Bình, Tp.HCM
                                        </li>


                                        <li>
                                            <i class="fa fa-envelope-o"></i> <a href="mailto:hi@haravan.com">hi@haravan.com</a>
                                        </li>


                                        <li>
                                            <i class="fa fa-phone"></i>1900.636.099
                                        </li>


                                        <li>
                                            <i class="fa fa-print"></i>1900.636.099
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6 col-md-2 col-xs-12 clear-sm">
                            <div class="widget-wrapper animated">

                                <h3 class="title title_left">Liên kết</h3>

                                <div class="inner">

                                    <ul class="list-unstyled list-styled">

                                        <li>
                                            <a href="/">Trang chủ</a>
                                        </li>

                                        <li>
                                            <a href="/collections/all">Sản phẩm</a>
                                        </li>

                                        <li>
                                            <a href="/blogs/news">Blog</a>
                                        </li>

                                        <li>
                                            <a href="/pages/about-us">Giới thiệu</a>
                                        </li>

                                        <li>
                                            <a href="/pages/lien-he">Liên hệ</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6 col-md-3 col-xs-12 clear-sm">
                            <div class="widget-wrapper animated">


                                <h3 class="title title_left">Đăng kí nhận tin</h3>

                                <div class="inner">


                                    <form accept-charset="UTF-8" action="/account/contact" class="contact-form" method="post">
                                        <input name="form_type" type="hidden" value="customer">
                                        <input name="utf8" type="hidden" value="✓">

                                        <div class="group-input">
                                            <input type="hidden" id="contact_tags" name="contact[tags]" value="khách hàng tiềm năng, bản tin">
                                            <input type="email" required="required" name="contact[email]" id="contact_email">
                                            <span class="bar"></span>
                                            <label>Nhập email của bạn</label>
                                            <button type="submit"><i class="fa fa-paper-plane-o"></i></button>
                                        </div>






{{--                                        <input id="07294a3209384ba08627b0eaf52842e5" name="g-recaptcha-response" type="hidden" value="03AGdBq24W_hwwBRbrxF9XKo0-wwsCIN_AelCUgHXa9d9cV6R0FMeBSY-4uwKypCYG7aVl_oU4lIYhL5SUTjKZCTCrbS1kPynFs_BY4xUzHh8uoetGlcY_C6hANHdXiI1vmpafONXIpxIjoML8F_1Y4Pxqb7ZxzfGUwxdEtRqmazDvYzaFtxeuIR-hwpja1xmQUyNpgdQkGI4rTHshK2hiQIjQTnXVzlZo6j60NIgPSVy2xo6ta7a2kU16cCKSoP74S3_TshxYhT0OSFIVSuXytjbMs07LdfoEl8H_hVPfJu1dVjx1fVxyrsHyV5V5SyvNUsSKxOU1oyB19dtw6FK_OY5w1aEdvy4dAHW_oRxV9Xf3HM2cewSy3CllqxdngaG9d2SDcSF7NW9slCcJvAl2C2AJXeyVq1tQhjuc-ZTaSzn9wAaEscgqxbjgj4T-xmqvlAiD02us_os4VDwpwqNyNrooutt4_tpVpw"><script src="https://www.google.com/recaptcha/api.js?render=6LdD18MUAAAAAHqKl3Avv8W-tREL6LangePxQLM-"></script><script>grecaptcha.ready(function() {grecaptcha.execute('6LdD18MUAAAAAHqKl3Avv8W-tREL6LangePxQLM-', {action: 'submit'}).then(function(token) {document.getElementById('07294a3209384ba08627b0eaf52842e5').value = token;});});</script></form>--}}


                                    <div class="caption">Hãy nhập email của bạn vào đây để nhận tin!</div>

                                </div>




{{--                                <div id="widget-social" class="social-icons">--}}
{{--                                    <ul class="list-inline">--}}

{{--                                        <li>--}}
{{--                                            <a target="_blank" href="/" class="social-wrapper facebook">--}}
{{--        <span class="social-icon">--}}
{{--          <i class="fa fa-facebook"></i>--}}
{{--        </span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}


{{--                                        <li>--}}
{{--                                            <a target="_blank" href="/" class="social-wrapper twitter">--}}
{{--        <span class="social-icon">--}}
{{--          <i class="fa fa-twitter"></i>--}}
{{--        </span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}


{{--                                        <li>--}}
{{--                                            <a target="_blank" href="/" class="social-wrapper pinterest">--}}
{{--        <span class="social-icon">--}}
{{--          <i class="fa fa-pinterest"></i>--}}
{{--        </span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}


{{--                                        <li>--}}
{{--                                            <a target="_blank" href="/" class="social-wrapper google">--}}
{{--        <span class="social-icon">--}}
{{--          <i class="fa fa-google-plus"></i>--}}
{{--        </span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}


{{--                                        <li>--}}
{{--                                            <a target="_blank" href="/" class="social-wrapper youtube">--}}
{{--        <span class="social-icon">--}}
{{--          <i class="fa fa-youtube"></i>--}}
{{--        </span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}


{{--                                        <li>--}}
{{--                                            <a target="_blank" href="/" class="social-wrapper instagram">--}}
{{--        <span class="social-icon">--}}
{{--          <i class="fa fa-instagram"></i>--}}
{{--        </span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}

{{--                                    </ul>--}}
{{--                                </div>--}}


                            </div>
                        </div>


                        <div class="col-sm-6 col-md-4 col-xs-12 clear-sm">
                            <div class="widget-wrapper animated">

                                <h3 class="title title_left">Kết nối với chúng tôi</h3>

                                <div class="inner">
                                    <!-- Facebook widget -->

                                    <div class="footer-static-content">
                                        <div class="fb-page fb_iframe_widget" data-href="https://www.facebook.com/haravan.official" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=263266547210244&amp;container_width=360&amp;height=300&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fharavan.official&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;show_posts=false&amp;small_header=false"><span style="vertical-align: bottom; width: 340px; height: 130px;"><iframe name="f326839e4c04d7c" width="1000px" height="300px" data-testid="fb:page Facebook Social Plugin" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.0/plugins/page.php?adapt_container_width=true&amp;app_id=263266547210244&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df162691237898f4%26domain%3Ddefault-baby.myharavan.com%26origin%3Dhttps%253A%252F%252Fdefault-baby.myharavan.com%252Ff148504297e0558%26relation%3Dparent.parent&amp;container_width=360&amp;height=300&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fharavan.official&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;show_posts=false&amp;small_header=false" style="border: none; visibility: visible; width: 340px; height: 130px;" class=""></iframe></span></div>
                                    </div>
                                    <div style="clear:both;">

                                    </div>

                                    <!-- #Facebook widget -->
{{--                                    <script>--}}
{{--                                        (function(d, s, id) {--}}
{{--                                            var js, fjs = d.getElementsByTagName(s)[0];--}}
{{--                                            if (d.getElementById(id)) return;--}}
{{--                                            js = d.createElement(s); js.id = id;--}}
{{--                                            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=263266547210244&version=v2.0";--}}
{{--                                            fjs.parentNode.insertBefore(js, fjs);--}}
{{--                                        }(document, 'script', 'facebook-jssdk'));--}}
{{--                                    </script>--}}

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="footer-copyright">
                <div class="container copyright">
                    <p>Copyright © 2021 default-baby. <a target="_blank" href="https://www.haravan.com">Powered by Haravan</a>.</p>

                </div>
            </div>
        </footer>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('script')
</body>
</html>
