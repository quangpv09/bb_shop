<div class="menu-category">
    <div class="title">
        <h3>Danh mục</h3>
    </div>
    <div class="list-item">
        <ul>
            <li class="item">
                <a href="#">Đồ chơi trẻ em</a>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
                <ul class="sub-list-item">
                    <li class="sub-item">
                        <a href="#">Đồ chơi mô hình</a>
                    </li>
                    <li class="sub-item">
                        <a href="#">Đồ chơi mô hình</a>
                    </li>
                    <li class="sub-item">
                        <a href="#">Đồ chơi mô hình</a>
                    </li>
                </ul>
            </li>
            <li class="item">
                <a href="#">Đồ chơi trẻ em</a>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
                <ul class="sub-list-item">
                    <li class="sub-item">
                        <a href="#">Đồ chơi mô hình</a>
                    </li>
                    <li class="sub-item">
                        <a href="#">Đồ chơi mô hình</a>
                    </li>
                    <li class="sub-item">
                        <a href="#">Đồ chơi mô hình</a>
                    </li>
                </ul>
            </li>
            <li class="item">
                <a href="#">Đồ chơi trẻ em</a>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </li>
        </ul>
    </div>
</div>
