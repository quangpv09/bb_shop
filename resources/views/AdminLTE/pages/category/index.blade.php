@extends('AdminLTE.index')

@section('content_admin')
<!-- Content Header (Page header) -->

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Category manage</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <a href="{{ route('admin.category.create') }}"><button type="button" class="btn btn-primary">Create</button></a>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
      <section class="content">
            <div class="container-fluid">
                <div class="row">
                  <div class="col-md-12">
                  @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                  @endif
                  <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Name</th>
                          <th scope="col">Slug</th>
                          <!-- <th scope="col">Parent</th> -->
                          <th scope="col">Created at</th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($categories as $k => $cat)
                            <tr>
                              <th scope="row">{{ $k + 1 }}</th>
                              <td>{{ $cat->name}}</td>
                              <td>{{ $cat->slug}}</td>
                              <!-- <td>{{ $cat->parent_name ? $cat->parent_name : ''}}</td> -->
                              <td>{{ $cat->created_at}}</td>
                              <td><a href="{{ route('admin.category.edit', ['id' => $cat->id]) }}"><button type="button" class="btn btn-info">Edit</button></a></td>
                              <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteCategoryModal-{{$cat->id}}">Delete</button></td>
                            </tr>
                            <div class="modal fade" id="deleteCategoryModal-{{$cat->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <!-- <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div> -->
                                <div class="modal-body" style="padding: 50px;">
                                  <h3>Are you sure you want to delete ?</h3>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" data-id="{{$cat->id}}" class="btn btn-danger submitDeleteCategory">Delete</button>
                                </div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                      </tbody>
                    </table>
                    {{ $categories->links() }}
                  </div>
                </div>
            </div><!-- /.container-fluid -->
          </section>
@endsection