@extends('AdminLTE.index')

@section('content_admin')
<div class="content-header">
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Edit product</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert" style="margin-top: 15px;">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
                <form method="POST" action="{{ route('admin.product.edit',['id' => $product->id]) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="categoryName">Name</label>
                        <input type="text" class="form-control" id="categoryName" name="name" value="{{ $product->name }}">
                    </div>
                    <div class="form-group">
                        <label for="categoryCode">Code</label>
                        <input type="text" class="form-control" id="categoryCode" name="code" value="{{ $product->code }}">
                    </div>
                    <div class="form-group">
                        <label for="categoryPrice">Price</label>
                        <input type="number" class="form-control" id="categoryPrice" name="price" value="{{ $product->price }}">
                    </div>
                    <div class="form-group">
                        <label for="categoryImage">Image</label>
                        <input type="file" onchange="readURL(this);" id="categoryImage" name="image">
                        <img style="width: 150px; height: 150px;" id="blah" src="{{ url('storage'.$product->image) }}" alt="your image" />
                    </div>
                    <div class="form-group">
                        <label for="categoryID">Category</label>
                        <select class="form-control" id="categoryID" name="category_id">
                            @foreach($categories as $cat)
                                <option {{$product->category_id == $cat->id ? 'selected' : ''}} value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="categoryDescription">Description</label>
                        <textarea id="editor" name="description">
                        {!! $product->description !!}
                            <!-- <p>Here goes the initial content of the editor.</p> -->
                        </textarea>
                    </div>
            
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
</div>
<script>
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection