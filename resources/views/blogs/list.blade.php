@extends('layouts.app')

@section('content')
    <div class="blog">
        <div class="header">
            <div class="wrap-breadcrumb">
                <div class="clearfix container">
                    <div class="row main-header">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5  ">
                            <ol class="breadcrumb breadcrumb-arrows">
                                <li><a href="/" target="_self">Trang chủ</a></li>


                                <li><a href="/collections" target="_self">Danh mục</a></li>



                                <li class="active"><span>Tất cả sản phẩm</span></li>



                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        @include('sidebar.sidebar')
                    </div>
                    <div class="col-md-9">
                        <div class="articles clearfix" id="layout-page">
                            <div class="col-md-12  col-sm-12 col-xs-12">
                                <h1>Tin tức</h1>
                            </div>

                            <!-- Begin: Nội dung blog -->






                            <div class="news-content row">

                                <div class="col-md-5 col-xs-12 col-sm-12 img-article">
                                    <div class="art-img">
                                        <img src="//file.hstatic.net/1000150333/article/aaaas_large.jpg" alt="">
                                    </div>
                                </div>


                                <div class=" col-md-7 col-sm-12  col-xs-12">
                                    <!-- Begin: Nội dung bài viết -->
                                    <h2 class="title-article"><a href="/blogs/news/kinh-nghiem-khi-chon-mua-xe-day-cho-be">Kinh nghiệm khi chọn mua xe đẩy cho bé</a></h2>
                                    <div class="body-content">
                                        <ul class="info-more">
                                            <li><i class="fa fa-calendar-o"></i><time pubdate="" datetime="2016-12-15">15/12/2016</time></li>
                                            <li><i class="fa fa-file-text-o"></i><a href="#"> Tin tức	</a> </li>

                                        </ul>
                                        <p>Không giống như bình sữa, quần áo chỉ sử dụng trong một thời gian ngắn, xe đẩy là một vật dụng gắn bó với bé yêu lâu dài, rất tiện lợi cho gia đình và ngày càng được nhiều Bố...</p>
                                    </div>
                                    <!-- End: Nội dung bài viết -->
                                    <a class="readmore btn-rb clear-fix" href="/blogs/news/kinh-nghiem-khi-chon-mua-xe-day-cho-be" role="button">Xem tiếp <span class="fa fa-angle-double-right"></span></a>
                                </div>


                            </div>
                            <hr class="line-blog">






                            <div class="news-content row">

                                <div class="col-md-5 col-xs-12 col-sm-12 img-article">
                                    <div class="art-img">
                                        <img src="//file.hstatic.net/1000150333/article/2_large.jpg" alt="">
                                    </div>
                                </div>


                                <div class=" col-md-7 col-sm-12  col-xs-12">
                                    <!-- Begin: Nội dung bài viết -->
                                    <h2 class="title-article"><a href="/blogs/news/phat-sot-voi-ve-xinh-dep-cua-thien-than-nhi-thai-lan">Phát sốt với vẻ xinh đẹp của thiên thần nhí Thái Lan</a></h2>
                                    <div class="body-content">
                                        <ul class="info-more">
                                            <li><i class="fa fa-calendar-o"></i><time pubdate="" datetime="2016-12-15">15/12/2016</time></li>
                                            <li><i class="fa fa-file-text-o"></i><a href="#"> Tin tức	</a> </li>

                                        </ul>
                                        <p>Cô bé sở hữu gương mặt xinh đẹp như thiên thần, đôi mắt to tròn, làn da trắng và cử chỉ đáng yêu.Nổi tiếng không kém các ngôi sao tại Thái Lan chính là bé gái xinh đẹp Jirada Jenna...</p>
                                    </div>
                                    <!-- End: Nội dung bài viết -->
                                    <a class="readmore btn-rb clear-fix" href="/blogs/news/phat-sot-voi-ve-xinh-dep-cua-thien-than-nhi-thai-lan" role="button">Xem tiếp <span class="fa fa-angle-double-right"></span></a>
                                </div>


                            </div>
                            <hr class="line-blog">






                            <div class="news-content row">

                                <div class="col-md-5 col-xs-12 col-sm-12 img-article">
                                    <div class="art-img">
                                        <img src="//file.hstatic.net/1000150333/article/3_large.jpg" alt="">
                                    </div>
                                </div>


                                <div class=" col-md-7 col-sm-12  col-xs-12">
                                    <!-- Begin: Nội dung bài viết -->
                                    <h2 class="title-article"><a href="/blogs/news/meo-chon-quan-ao-tranh-di-ung-cho-tre">Mẹo chọn quần áo tránh dị ứng cho trẻ</a></h2>
                                    <div class="body-content">
                                        <ul class="info-more">
                                            <li><i class="fa fa-calendar-o"></i><time pubdate="" datetime="2016-12-15">15/12/2016</time></li>
                                            <li><i class="fa fa-file-text-o"></i><a href="#"> Tin tức	</a> </li>

                                        </ul>
                                        <p>Chọn mua quần áo cho bé không chỉ là đáp ứng nhu cầu, sở thích của bé mà còn phải phù hợp với túi tiền của Mẹ. Do đó, không ít các bậc phụ huynh có suy nghĩ đơn giản...</p>
                                    </div>
                                    <!-- End: Nội dung bài viết -->
                                    <a class="readmore btn-rb clear-fix" href="/blogs/news/meo-chon-quan-ao-tranh-di-ung-cho-tre" role="button">Xem tiếp <span class="fa fa-angle-double-right"></span></a>
                                </div>


                            </div>
                            <hr class="line-blog">






                            <div class="news-content row">

                                <div class="col-md-5 col-xs-12 col-sm-12 img-article">
                                    <div class="art-img">
                                        <img src="//file.hstatic.net/1000150333/article/4_large.jpg" alt="">
                                    </div>
                                </div>


                                <div class=" col-md-7 col-sm-12  col-xs-12">
                                    <!-- Begin: Nội dung bài viết -->
                                    <h2 class="title-article"><a href="/blogs/news/quan-ao-moi-doc-hai-cho-suc-khoe-cua-tre">Quần áo mới độc hại cho sức khỏe của trẻ?</a></h2>
                                    <div class="body-content">
                                        <ul class="info-more">
                                            <li><i class="fa fa-calendar-o"></i><time pubdate="" datetime="2016-12-15">15/12/2016</time></li>
                                            <li><i class="fa fa-file-text-o"></i><a href="#"> Tin tức	</a> </li>

                                        </ul>
                                        <p>Bình thường chúng ta chỉ quan tâm tới an toàn thực phẩm, còn chuyện mặc chỉ để ý tới mẫu mã kiểu dáng mà bỏ quan vấn đề sức khỏe tiềm ẩn. Dưới đây là những lý do không nên...</p>
                                    </div>
                                    <!-- End: Nội dung bài viết -->
                                    <a class="readmore btn-rb clear-fix" href="/blogs/news/quan-ao-moi-doc-hai-cho-suc-khoe-cua-tre" role="button">Xem tiếp <span class="fa fa-angle-double-right"></span></a>
                                </div>


                            </div>
                            <hr class="line-blog">


                            <!-- End: Nội dung blog -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
{{--    <script src="{{ asset('js/categories.js') }}"></script>--}}
@endsection
