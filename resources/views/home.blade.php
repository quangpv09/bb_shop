@extends('layouts.app')

@section('content')
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="https://hstatic.net/333/1000150333/1000203368/slideshow_1.jpg?v=21" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="https://hstatic.net/333/1000150333/1000203368/slideshow_2.jpg?v=21" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="https://hstatic.net/333/1000150333/1000203368/slideshow_3.jpg?v=21" alt="Third slide">
                    </div>
                </div>
            </div>
    <div class="hone-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="list-product">
                        <div class="list-product-highlight">
                            <aside class="styled_header  use_icon ">
                                <h2>What hot</h2>

                                <h3>Sản phẩm nổi bật</h3>
                                <span class="icon"><img src="https://hstatic.net/333/1000150333/1000203368/icon_featured.png?v=21" alt=""></span>

                            </aside>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 pull-center center">
                                    <a class="btn btn-readmore" href="/collections/hot-products" role="button">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="animation fade-in home-banner-1">
                                    <aside class="banner-home-1">
                                        <div class="divcontent"><span class="ad_banner_1">Miễn phí vận chuyển<strong class="ad_banner_2">Với tất cả đơn hàng trên 500k</strong></span>
                                            <span class="ad_banner_desc">Miễn phí 2 ngày vận chuyển với đơn hàng trên 500k trừ trực tiếp khi thanh toán</span>
                                        </div>
                                        <div class="divstyle"></div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                        <div class="list-product-new">
                            <aside class="styled_header  use_icon ">


                                <h3>Sản phẩm mới</h3>
                                <span class="icon"><img src="//hstatic.net/333/1000150333/1000203368/icon_sale.png?v=21" alt="Newest trends"></span>

                            </aside>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-block" style="height: 336px">
                                        <div class="product-img" style="height: 261px">
                                            <a href="#">
                                                <img src="https://product.hstatic.net/1000150333/product/upload_bf53a04288b54b1fa6deab75273a6ce5_large.jpg">
                                            </a>
                                            <div class="actionss">
                                                <div class="btn-cart-products">
                                                    <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814282)">
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="view-details">
                                                    <a href="/products/ao-so-sinh-cai-giua-tay-ngan-carrot" class="view-detail">
                                                        <span><i class="fa fa-clone"> </i></span>
                                                    </a>
                                                </div>
                                                <div class="btn-quickview-products">
                                                    <a href="javascript:void(0);" class="quickview" data-handle="/products/ao-so-sinh-cai-giua-tay-ngan-carrot"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-detail">
                                            <h3 class="pro-name">
                                                <a href="#">Áo ghi lê bé trai PaPa </a>
                                            </h3>
                                            <div class="pro-prices">
                                                <p>39,000₫</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 pull-center center">
                                    <a class="btn btn-readmore" href="/collections/hot-products" role="button">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <div class="banner-bottom">
                            <div class="row">

                                <div class="col-xs-12 col-sm-6 home-category-item-2">
                                    <div class="block-home-category">
                                        <a href="collections/hot-products">
                                            <img class="b-lazy b-loaded" src="https://hstatic.net/333/1000150333/1000203368/block_home_category1.jpg?v=21" alt="Thời trang nữ">
                                        </a>
                                    </div>
                                </div>


                                <div class="col-xs-12 col-sm-6 home-category-item-3">
                                    <div class="block-home-category">
                                        <a href="collections/hot-products">
                                            <img class="b-lazy b-loaded" src="https://hstatic.net/333/1000150333/1000203368/block_home_category2.jpg?v=21" alt="Thời trang nam">
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
