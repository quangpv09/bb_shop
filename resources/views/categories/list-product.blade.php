@extends('layouts.app')

@section('content')
    <div class="category">
        <div class="header">
            <div class="wrap-breadcrumb">
                <div class="clearfix container">
                    <div class="row main-header">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5  ">
                            <ol class="breadcrumb breadcrumb-arrows">
                                <li><a href="/" target="_self">Trang chủ</a></li>


                                <li><a href="/collections" target="_self">Danh mục</a></li>



                                <li class="active"><span>Tất cả sản phẩm</span></li>



                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        @include('sidebar.sidebar')
                    </div>
                    <div class="col-md-9">
                            <div class="sort-collection">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h1>
                                            Tất cả sản phẩm
                                        </h1>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="browse-tags">
                                            <span>Sắp xếp theo:</span>
                                            <span class="custom-dropdown custom-dropdown--white">
										<select class="sort-by custom-dropdown__select custom-dropdown__select--white">

											<option value="price-ascending">Giá: Tăng dần</option>
											<option value="price-descending">Giá: Giảm dần</option>
											<option value="title-ascending">Tên: A-Z</option>
											<option value="title-descending">Tên: Z-A</option>
											<option value="created-ascending">Cũ nhất</option>
											<option value="created-descending">Mới nhất</option>
											<option value="best-selling">Bán chạy nhất</option>
										</select>
									</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 content-product-list">
                                <div class="row product-list">


                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
















                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">



                                                <a href="/products/xe-truot-hdl" title="Xe trượt HDL">
                                                    <img class="first-image  has-img" alt=" Xe trượt HDL " src="//product.hstatic.net/1000150333/product/upload_4bb7319a4d4146309d1c6ad256286999_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_f4d6260f9c484471ad30d72b2fd83770_large.jpg" alt=" Xe trượt HDL ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814358)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/xe-truot-hdl" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/xe-truot-hdl"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="product-detail clearfix">


                                                <!-- sử dụng pull-left -->
                                                <h3 class="pro-name"><a href="/products/xe-truot-hdl" title="Xe trượt HDL">Xe trượt HDL </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">435,000₫</p>
                                                    <p class="pro-price-del text-left"></p>


                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">


















                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">



                                                <a href="/products/xe-truot-3-banh" title="Xe trượt 3 bánh">
                                                    <img class="first-image  has-img" alt=" Xe trượt 3 bánh " src="//product.hstatic.net/1000150333/product/upload_f2b057cc8c454700a6cfd54fd6a27fc6_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_ec9e16203b47424a9a23e50fc8f635fc_large.jpg" alt=" Xe trượt 3 bánh ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814355)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/xe-truot-3-banh" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/xe-truot-3-banh"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="product-detail clearfix">


                                                <!-- sử dụng pull-right -->
                                                <h3 class="pro-name"><a href="/products/xe-truot-3-banh" title="Xe trượt 3 bánh">Xe trượt 3 bánh </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">1,200,000₫</p>
                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">


















                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">




                                                <a href="/products/xe-tap-thang-bang-cruzee" title="Xe tập thăng bằng Cruzee">
                                                    <img class="first-image  has-img" alt=" Xe tập thăng bằng Cruzee " src="//product.hstatic.net/1000150333/product/upload_cc00af45dbe94e8cb4d19a05f57970f6_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_8b188104973f48069b553981cc9ec8a7_large.jpg" alt=" Xe tập thăng bằng Cruzee ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814352)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/xe-tap-thang-bang-cruzee" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/xe-tap-thang-bang-cruzee"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="product-detail clearfix">


                                                <!-- sử dụng pull-left -->
                                                <h3 class="pro-name"><a href="/products/xe-tap-thang-bang-cruzee" title="Xe tập thăng bằng Cruzee">Xe tập thăng bằng Cruzee </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">3,300,000₫</p>
                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
















                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">



                                                <a href="/products/xe-tap-di-go-lovelykid" title="Xe tập đi gỗ Lovelykid">
                                                    <img class="first-image  has-img" alt=" Xe tập đi gỗ Lovelykid " src="//product.hstatic.net/1000150333/product/upload_0dd04450f7694280ae3315e24e4256ce_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_5497d21a25894f40801e62c4a8f5db8d_large.jpg" alt=" Xe tập đi gỗ Lovelykid ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814350)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/xe-tap-di-go-lovelykid" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/xe-tap-di-go-lovelykid"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="product-detail clearfix">


                                                <!-- sử dụng pull-right -->
                                                <h3 class="pro-name"><a href="/products/xe-tap-di-go-lovelykid" title="Xe tập đi gỗ Lovelykid">Xe tập đi gỗ Lovelykid </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">290,000₫</p>
                                                    <p class="pro-price-del text-left"></p>


                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
















                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">



                                                <a href="/products/xe-day-tobby-aztec" title="Xe đẩy Tobby Aztec">
                                                    <img class="first-image  has-img" alt=" Xe đẩy Tobby Aztec " src="//product.hstatic.net/1000150333/product/upload_dc2361726c154258a509ff931ab7db73_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_dda973fe14574c80a6a4c0ed684e2bc3_large.jpg" alt=" Xe đẩy Tobby Aztec ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814347)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/xe-day-tobby-aztec" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/xe-day-tobby-aztec"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="product-detail clearfix">


                                                <!-- sử dụng pull-left -->
                                                <h3 class="pro-name"><a href="/products/xe-day-tobby-aztec" title="Xe đẩy Tobby Aztec">Xe đẩy Tobby Aztec </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">990,000₫</p>
                                                    <p class="pro-price-del text-left"></p>


                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
















                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">



                                                <a href="/products/xe-day-hai-chieu-tobby-zoo" title="Xe đẩy hai chiều Tobby Zoo">
                                                    <img class="first-image  has-img" alt=" Xe đẩy hai chiều Tobby Zoo " src="//product.hstatic.net/1000150333/product/upload_af53f139d1324fbaa19c3a89f24eb8fa_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_3bf43d4d91af43cb930874071e4f378a_large.jpg" alt=" Xe đẩy hai chiều Tobby Zoo ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814342)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/xe-day-hai-chieu-tobby-zoo" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/xe-day-hai-chieu-tobby-zoo"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="product-detail clearfix">


                                                <!-- sử dụng pull-right -->
                                                <h3 class="pro-name"><a href="/products/xe-day-hai-chieu-tobby-zoo" title="Xe đẩy hai chiều Tobby Zoo">Xe đẩy hai chiều Tobby Zoo </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">1,750,000₫</p>
                                                    <p class="pro-price-del text-left"></p>


                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
















                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">



                                                <a href="/products/xe-day-goodbaby" title="Xe đẩy Goodbaby">
                                                    <img class="first-image  has-img" alt=" Xe đẩy Goodbaby " src="//product.hstatic.net/1000150333/product/upload_dc84aed7c9574b28bd3432c527a1f4fc_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_7199643caeb44d2497fd5bad787fe760_large.jpg" alt=" Xe đẩy Goodbaby ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814340)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/xe-day-goodbaby" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/xe-day-goodbaby"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="product-detail clearfix">


                                                <!-- sử dụng pull-left -->
                                                <h3 class="pro-name"><a href="/products/xe-day-goodbaby" title="Xe đẩy Goodbaby">Xe đẩy Goodbaby </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">3,500,000₫</p>
                                                    <p class="pro-price-del text-left"></p>


                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
















                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">



                                                <a href="/products/xe-day-geoby" title="Xe đẩy Geoby">
                                                    <img class="first-image  has-img" alt=" Xe đẩy Geoby " src="//product.hstatic.net/1000150333/product/upload_5f6a2dbeff954313a105d99e3266ea20_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_96bf3eb6fe2f4590a121c0ef34b64d00_large.jpg" alt=" Xe đẩy Geoby ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814338)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/xe-day-geoby" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/xe-day-geoby"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="product-detail clearfix">


                                                <!-- sử dụng pull-right -->
                                                <h3 class="pro-name"><a href="/products/xe-day-geoby" title="Xe đẩy Geoby">Xe đẩy Geoby </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">4,500,000₫</p>
                                                    <p class="pro-price-del text-left"></p>


                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
















                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">



                                                <a href="/products/vo-so-sinh-i-love-mom" title="Vớ sơ sinh I love Mom">
                                                    <img class="first-image  has-img" alt=" Vớ sơ sinh I love Mom " src="//product.hstatic.net/1000150333/product/upload_50a92e082375472f8ca2d83c6d8c1c14_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_d0e07b477e4849e8aa886c6f6132d80f_large.jpg" alt=" Vớ sơ sinh I love Mom ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814336)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/vo-so-sinh-i-love-mom" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/vo-so-sinh-i-love-mom"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="product-detail clearfix">


                                                <!-- sử dụng pull-left -->
                                                <h3 class="pro-name"><a href="/products/vo-so-sinh-i-love-mom" title="Vớ sơ sinh I love Mom">Vớ sơ sinh I love Mom </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">9,000₫</p>
                                                    <p class="pro-price-del text-left"></p>


                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
















                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">



                                                <a href="/products/vo-so-sinh-i-love-dad" title="Vớ sơ sinh I love Dad">
                                                    <img class="first-image  has-img" alt=" Vớ sơ sinh I love Dad " src="//product.hstatic.net/1000150333/product/upload_cb574540933047d18f6881e11c110d5d_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_30565bf2f3f849c58f5bd3e51b9a40de_large.jpg" alt=" Vớ sơ sinh I love Dad ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814334)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/vo-so-sinh-i-love-dad" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/vo-so-sinh-i-love-dad"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-detail clearfix">
                                                <!-- sử dụng pull-right -->
                                                <h3 class="pro-name"><a href="/products/vo-so-sinh-i-love-dad" title="Vớ sơ sinh I love Dad">Vớ sơ sinh I love Dad </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">9,000₫</p>
                                                    <p class="pro-price-del text-left"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">
                                                <a href="/products/ta-vai-dan-carrot" title="Tã vải dán Carrot">
                                                    <img class="first-image  has-img" alt=" Tã vải dán Carrot " src="//product.hstatic.net/1000150333/product/upload_798602ed932944e986f2fa6a8b671bdc_large.jpg">

                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_67c61aecc47b40ebb7f3d5fa0d89dc3a_large.jpg" alt=" Tã vải dán Carrot ">

                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814330)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/ta-vai-dan-carrot" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/ta-vai-dan-carrot"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-detail clearfix">
                                                <!-- sử dụng pull-left -->
                                                <h3 class="pro-name"><a href="/products/ta-vai-dan-carrot" title="Tã vải dán Carrot">Tã vải dán Carrot </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">56,000₫</p>
                                                    <p class="pro-price-del text-left"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4  col-sm-6 col-xs-12 pro-loop">
                                        <div class="product-block product-resize fixheight" style="height: 335px;">
                                            <div class="product-img image-resize view view-third" style="height: 260px;">
                                                <a href="/products/ta-dan-papa" title="Tã dán PaPa">
                                                    <img class="first-image  has-img" alt=" Tã dán PaPa " src="//product.hstatic.net/1000150333/product/upload_e25dd735a428428b8bf2f471e4a1603b_large.jpg">
                                                    <img class="second-image" src="//product.hstatic.net/1000150333/product/upload_c3901e39acb249eaa6b638e3cf0ffa3c_large.jpg" alt=" Tã dán PaPa ">
                                                </a>
                                                <div class="actionss">
                                                    <div class="btn-cart-products">
                                                        <a href="javascript:void(0);" onclick="add_item_show_modalCart(1009814326)">
                                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view-details">
                                                        <a href="/products/ta-dan-papa" class="view-detail">
                                                            <span><i class="fa fa-clone"> </i></span>
                                                        </a>
                                                    </div>
                                                    <div class="btn-quickview-products">
                                                        <a href="javascript:void(0);" class="quickview" data-handle="/products/ta-dan-papa"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-detail clearfix">
                                                <!-- sử dụng pull-right -->
                                                <h3 class="pro-name"><a href="/products/ta-dan-papa" title="Tã dán PaPa">Tã dán PaPa </a></h3>
                                                <div class="pro-prices">
                                                    <p class="pro-price">39,000₫</p>
                                                    <p class="pro-price-del text-left"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('js/categories.js') }}"></script>
@endsection
