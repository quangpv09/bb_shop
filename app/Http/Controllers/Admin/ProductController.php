<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    //
    public function index() {
        $products = Product::paginate(10);
        foreach($products as $prod) {
            $prod->category = Product::where('id', $prod->id)->first()->name;
        }
        return view('AdminLTE.pages.product.index', ['products' => $products]);
    }

    public function formCreate() {
        $categories = Category::all();
        return view('AdminLTE.pages.product.create', ['categories' => $categories]);
    }

    public function create(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:255',
            'price' => 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'category_id' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.product.formCreate')
                        ->withErrors($validator)
                        ->withInput();
        }

        $imageName = time().'.'.$request->image->extension();
        $request->image->storeAs('public/images', $imageName);
        $filePath = '/images/'.$imageName;
        $data['image'] = $filePath;
        $data['slug'] = Str::slug($data['name'], '-');
        Product::create($data);
        return redirect()->route('admin.product.index')->with('success', 'Create product success !');
    }

    public function formEdit($id) {
        $categories = Category::all();
        $product = Product::find($id);
        return view('AdminLTE.pages.product.edit', ['categories' => $categories, 'product' => $product]);
    }

    public function edit(Request $request, $id) {
        $data = $request->all();
        unset($data['_token']);
        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:255',
            'price' => 'required|string|max:255',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'category_id' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.product.formEdit')
                        ->withErrors($validator)
                        ->withInput();
        }

        if(isset($data['image'])) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->storeAs('public/images', $imageName);
            $filePath = '/images/'.$imageName;
            $data['image'] = $filePath;
        }
        $data['slug'] = Str::slug($data['name'], '-');
         Product::where('id', $id)->update($data);
        return redirect()->route('admin.product.index')->with('success', 'Update product success !');
    }
    
    public function delete(Request $request) {
        $id = $request->id;
        Product::where('id', $id)->delete();
        return response()->json([
            'error' => false,
        ]);
    }
}
