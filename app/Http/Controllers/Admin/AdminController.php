<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    //

    public function __construct()
   {
       $this->middleware('auth');
   }

    public function index() {
        if(Auth::user()->role == User::USER_ROLE)
        abort(401);
        return view('AdminLTE.pages.home');
    }
}
