<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    //
    public function index() {
        $categories = Category::paginate(5);
        // foreach ($categories as $cat) {
        //     if($cat->parent_id) {
        //         $cat->parent_name = Category::where('id', $cat->parent_id)->first()->name;
        //     }
        // }
        return view('AdminLTE.pages.category.index', ['categories' => $categories]);
    }

    public function formCreate() {
        $categories = Category::whereNull('parent_id')->get();
        return view('AdminLTE.pages.category.create', ['categories' => $categories]);       
    }

    public function create(Request $request) {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.category.formCreate')
                        ->withErrors($validator)
                        ->withInput();
        }
        Category::create([
            'name' => $data['name'],
            'slug' => Str::slug($data['name'], '-'),
            'parent_id' => $data['parent_id'] ? $data['parent_id'] : null
        ]);
        return redirect()->route('admin.category.index')->with('success', 'Create category success !');
    }

    public function formEdit($id) {
        $category = Category::where('id', $id)->first();
        $categories = Category::whereNull('parent_id')->get();
        return view('AdminLTE.pages.category.edit', ['category' => $category, 'categories' => $categories]);
    }

    public function edit(Request $request, $id) {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:categories,name,'.$id,
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.category.formEdit', ['id' => $id])
                        ->withErrors($validator)
                        ->withInput();
        }
        Category::where('id', $id)->update([
            'name' => $data['name'],
            'slug' => Str::slug($data['name'], '-'),
            'parent_id' => $data['parent_id'] ? $data['parent_id'] : null
        ]);
        return redirect()->route('admin.category.index')->with('success', 'Update category success !');
    }

    public function delete(Request $request) {
        $id = $request->id;
        Category::where('id', $id)->delete();
        return response()->json([
            'error' => false,
        ]);
    }
}
