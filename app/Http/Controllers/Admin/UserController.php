<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function index() {
        $users = User::paginate(10);
        return view('AdminLTE.pages.user.index', ['users' => $users]);
    }

    public function formCreate() {
        return view('AdminLTE.pages.user.create');
    }

    public function create(Request $request) {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|max:255',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'password' => 'required|min:8',
            'role' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.user.formCreate')
                        ->withErrors($validator)
                        ->withInput();
        }
        User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role' => $data['role']
        ]);
        return redirect()->route('admin.user.index')->with('success', 'Create user success !');
    }

    public function formEdit($id) {
        $user = User::find($id);
        return view('AdminLTE.pages.user.edit', ['user' => $user]);
    }

    public function edit(Request $request, $id) {
        $data = $request->all();
        $validator = Validator::make($data, [
            'email' => 'required|max:255|unique:users,email,'.$id,
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'password' => 'required|min:8',
            'role' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.user.formEdit', ['id' => $id])
                        ->withErrors($validator)
                        ->withInput();
        }
        $user = User::find($id);
        $data = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'role' => $data['role']
        ];
        if( $data['password'] == $user->password ) {
            unset($data['password']);
        } else {
            $data['password'] = Hash::make($data['password']);
        }
        User::where('id', $id)->update($data);
        return redirect()->route('admin.user.index')->with('success', 'Update user success !');
    }


    public function delete(Request $request) {
        $id = $request->id;
        User::where('id', $id)->delete();
        return response()->json([
            'error' => false,
        ]);
    }
}
